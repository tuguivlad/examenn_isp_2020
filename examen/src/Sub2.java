
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.util.*;

public class Sub2 extends JFrame {


    JLabel Text1, Text2;
    JTextField tTextField1, tTextField2;
    JTextArea tArea;
    JButton bTest;

    Sub2() {


        setTitle("Examen");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        Text1 = new JLabel("TextField 1 ");
        Text1.setBounds(10, 50, width, height);

        Text2 = new JLabel("TextField 2 ");
        Text2.setBounds(10, 100, width, height);

        tTextField1 = new JTextField();
        tTextField1.setBounds(70, 50, width, height);

        tTextField2 = new JTextField();
        tTextField2.setBounds(70, 100, width, height);

        bTest = new JButton("BUTON");
        bTest.setBounds(10, 150, width, height);

        bTest.addActionListener(new TratareButon());

        tArea = new JTextArea();
        tArea.setBounds(10, 180, 150, 80);
        Sub2.this.tTextField1.setText(" ");
        Sub2.this.tTextField2.setText(" ");

        add(Text1);
        add(Text2);
        add(tTextField1);
        add(tTextField2);
        add(bTest);
        add(tArea);

    }

    public static void main(String[] args) {
        new Sub2();
    }

    class TratareButon implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String text1 = Sub2.this.tTextField1.getText();
            String text2 = Sub2.this.tTextField2.getText();
            String textpus = "examen";
            if ((text1.equals(" ")) && (text2.equals(" "))) {
                Sub2.this.tTextField1.setText(textpus);
            } else if (text1.equals(textpus)) {
                Sub2.this.tTextField2.setText(textpus);
                Sub2.this.tTextField1.setText(" ");
            } else if (text2.equals(textpus)) {
                Sub2.this.tTextField1.setText(textpus);
                Sub2.this.tTextField2.setText(" ");
            }
        }

    }

}